import React, {Component} from 'react';
import './App.css';
import Button from '@material-ui/core/Button';

class App extends Component {

  state = {
    planets: [],
    size: [],
    counter: 1
  };

  addMore = () => {
    this.componentDidMount() 
    this.setState({
      counter: this.state.counter + 1
    });
  }
  
  render () {
    return (
      <div className="App-logo">
        <header className="card align-center App-header">
          <br/><br/>
          <div className="center">
          <h1>{this.state.planets.name}</h1>
          <div>
            <p>POPULATION: {this.state.planets.population}</p>
            <p>CLIMATE: {this.state.planets.climate}</p>
            <p>TERRAIN: {this.state.planets.terrain}</p>
            <p className="p1">FEATURED IN FILMS: {this.state.size}</p>
          </div>
          </div>
          <div className="btnRandom">
            <Button variant="contained" color="primary" onClick={ this.addMore }>
              New Planet
            </Button>
          </div>

          <p>Planets counter : &nbsp;  
            { this.state.counter }
          </p>
        </header>
        <small className="link">Copyright &copy; 2019 &nbsp;
            <a href="https://www.linkedin.com/in/fabio-francisco-campelo-51ba75110" target="_blank" className="link_font">
              Fábio Campêllo
            </a>
          </small> 
      </div>
    );
  }

  componentDidMount() {
    let numberId = Math.floor(Math.random() * (61 - 1) + 1)
    fetch(`https://swapi.co/api/planets/${numberId}/`)
    .then(res => res.json())
    .then((data) => {
      this.setState({ planets: data })
      this.setState({ size: this.state.planets.films.length })
      console.log(this.state.planets)
      console.log(this.state.planets.films.length )
    })
    .catch(console.log)
  }

}

export default App;
